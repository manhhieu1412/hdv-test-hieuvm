### Bài Test 2 

Khi em triển khai thống em sẽ làm mỗi trường 1 database. Như vậy sẽ quy được về bài toán đơn giản hơn.


![Screenshot](assets/diagram.png)

```
openDatabase("bachkhoa_2018.db")

SELECT T.IDStudent, T1.Name, T1.IDClass, T.Score FROM Score T INNER JOIN Student T1 ON T.IDStudent = T1.IDStudent WHERE T.Score >= 9 ( AND IDClass = X ~ để filter các bạn cùng lớp. X: là id của Class)

closeDatabase()
```