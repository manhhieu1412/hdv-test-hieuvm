package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by hieuvm on 3/2/18.
 * *
 */
public class Graph {

    private List<Node> nodes = new ArrayList<>();
    private List<Edge> edges = new ArrayList<>();

    private Node source;
    private Node destination;

    private boolean solved = false;

    public boolean isNodeReachable(Node node) {
        for (Edge edge : edges) {
            if (node == edge.getOne() || node == edge.getTwo()) {
                return true;
            }
        }

        return false;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    public Collection<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public void addEdge(Edge edge) {
        if (!edges.contains(edge)) {
            edges.add(edge);
        }
    }

    public Node getSource() {
        return source;
    }

    public void setSource(Node node) {
        if (nodes.contains(node)) {
            this.source = node;
        }
    }

    public Node getDestination() {
        return destination;
    }

    public void setDestination(Node node) {
        if (nodes.contains(node)) {
            this.destination = node;
        }
    }

    public void clear() {
        nodes.clear();
        edges.clear();
        solved = false;

        source = null;
        destination = null;
    }

}
