package model;

import com.sun.istack.internal.NotNull;

/**
 * Created by hieuvm on 3/2/18.
 * *
 */
public class Edge {
    private Node one;
    private Node two;
    private int distance = 1;

    public Edge(@NotNull Node one, @NotNull Node two) {
        this(one, two, 1);
    }

    public Edge(Node one, Node two, int distance) {
        this.one = one;
        this.two = two;
        this.distance = distance;
    }

    public Node getOne() {
        return one;
    }

    public Node getTwo() {
        return two;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return (one == edge.one && two == edge.two) || (one == edge.two && two == edge.one);
    }

    @Override
    public String toString() {
        return "Edge "
                + getOne().getId() + " - "
                + getTwo().getId();
    }
}
