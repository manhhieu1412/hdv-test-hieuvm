package model;

/**
 * Created by hieuvm on 3/2/18.
 * *
 */
public class Node {
    private int id;
//    private List<Node> shortestPath = new LinkedList<>();
//    private Map<Node, Integer> adjacentNodes = new HashMap<>();


    private Node() {
    }

    public Node(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

  /*  public void addDestination(Node destination, int distance) {
        adjacentNodes.put(destination, distance);
    }
*/

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return id == node.id;
    }
}
