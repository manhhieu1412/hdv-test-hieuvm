package main;

import model.Edge;
import model.Node;
import util.CSVUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hieuvm on 3/2/18.
 * *
 */
final class DataSample {

    static final Map<Integer, Node> ID_NODE_MAP = new HashMap<>();
    static final List<Edge> EDGES = new ArrayList<>();

    static {
        generateDataSample();
    }

    private static void generateDataSample() {
        String separate = ",";
        String pathname = "data.csv";
        int sampleSize = 3;
        
        List<String[]> data = CSVUtil.readCSV(new File(pathname), separate);

        for (String[] element : data) {
            if (element == null) {
                throw new RuntimeException("The data is invalid");
            }

            if (element.length < sampleSize) {
                throw new RuntimeException("The data is invalid");
            }

            Node one = createNode(Integer.parseInt(element[0].trim()));
            Node two = createNode(Integer.parseInt(element[1].trim()));
            int distance = Integer.parseInt(element[2].trim());

            EDGES.add(new Edge(one, two, distance));
        }
    }

    private static Node createNode(int id) {
        if (!ID_NODE_MAP.containsKey(id)) {
            ID_NODE_MAP.put(id, new Node(id));
        }
        return ID_NODE_MAP.get(id);
    }
}
