package main;

import algorithm.DijkstraAlgorithm;
import model.Graph;

import java.util.ArrayList;

import static main.DataSample.EDGES;
import static main.DataSample.ID_NODE_MAP;

/**
 * Created by hieuvm on 3/2/18.
 * *
 */
public class Main {

    private static final int SOURCE_ID = 13;
    private static final int DESTINATION_ID = 5;

    public static void main(String[] args) {

        Graph graph = new Graph();

        graph.setNodes(new ArrayList<>(ID_NODE_MAP.values()));
        graph.setSource(ID_NODE_MAP.get(SOURCE_ID));
        graph.setDestination(ID_NODE_MAP.get(DESTINATION_ID));

        graph.setEdges(EDGES);


        DijkstraAlgorithm algorithm = new DijkstraAlgorithm(graph);
        algorithm.run();

        System.out.println(algorithm.getDestinationPath());
    }


}
