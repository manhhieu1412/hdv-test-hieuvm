package util;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hieuvm on 3/4/18.
 * *
 */
public class CSVUtil {
    public static List<String[]> readCSV(File file, String separate) {
        try {
            return Files.lines(file.toPath(), StandardCharsets.UTF_8)
                    .map(line -> line.split(separate))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
