package algorithm;

import model.Graph;

/**
 * Created by hieuvm on 3/2/18.
 * *
 */
abstract class ShortestPathAlgorithm {

    protected Graph graph;

    ShortestPathAlgorithm(Graph graph) {
        this.graph = graph;
    }
}
