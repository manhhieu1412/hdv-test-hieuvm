package reporter;

import model.ChatRoomLog;
import reporter.collection.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by hieuvm on 3/4/18.
 * *
 */
public class Reporter {

    private static final String ACTION_ENTER = "ENTER";

    private final File mFile;

    private final DailyReport mDailyReport;
    private final HourlyReport mHourlyReport;
    private final WeeklyReport mWeeklyReport;

    public Reporter(File file) {
        this.mFile = file;
        mDailyReport = new DailyReport();
        mHourlyReport = new HourlyReport();
        mWeeklyReport = new WeeklyReport();
    }

    private void process(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; ) {
                processLine(line);
            }
            br.close();
        }

        mWeeklyReport.collect(mDailyReport.getResult());
    }

    private void processLine(String line) {
        // System.out.println(line);
        String regex = ",";
        int reportSize = 3;
        String[] rawData = line.split(regex);
        if (rawData.length < reportSize) {
            return;
        }

        String action = rawData[2].trim();

        if (!action.equalsIgnoreCase(ACTION_ENTER)) {
            return;
        }

        String time = rawData[0].trim();

        if (time.isEmpty()) {
            return;
        }

        String name = rawData[1].trim();

        ChatRoomLog log = new ChatRoomLog(time, name, action);

        mHourlyReport.collect(log);
        mDailyReport.collect(log);
    }

    private void show() {
        mHourlyReport.show();
        mDailyReport.show();
        mWeeklyReport.show();
    }

    public void run() throws IOException {
        process(mFile);
        show();
    }

}
