package reporter.collection;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by hieuvm on 3/5/18.
 * *
 */
final public class WeeklyReport {
    private Map<Integer, Integer> mapWeekOfMonth = new TreeMap<>();
    private String mMonth = "";

    public void collect(Map<String, Integer> log) {
        log.forEach((date, number) -> {
            int wk = hash(date);
            if (wk >= 1) {
                int count = number;
                if (mapWeekOfMonth.containsKey(wk)) {
                    count += mapWeekOfMonth.get(wk);
                }

                mapWeekOfMonth.put(wk, count);

            }
        });

        Map.Entry<String, Integer> entry = log.entrySet().iterator().next();
        String key = entry.getKey();
        mMonth = key.substring(0, 7);
    }

    public Map<Integer, Integer> getResult() {
        return mapWeekOfMonth;
    }

    public void show() {

        System.out.println("\nWeekly Report\n");

        String leftAlignFormat = "| %-15s | %-15s | %-15d |%n";
        String separate = "+-----------------+-----------------+-----------------+%n";

        System.out.format(separate);
        System.out.format("| Time            | Week            | Number Of ENTER |%n");
        System.out.format(separate);
        mapWeekOfMonth.forEach((week, number) ->
                System.out.format(leftAlignFormat, mMonth, week, number)
        );
        System.out.format(separate);
    }

    private int hash(String date) {
        int numberDay = 7;

        try {
            String day = date.substring(date.length() - 2);
            return Integer.parseInt(day) / numberDay + 1;
        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            return -1;
        }
    }


}
