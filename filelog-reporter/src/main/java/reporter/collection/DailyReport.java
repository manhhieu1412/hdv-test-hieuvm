package reporter.collection;

import model.ChatRoomLog;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by hieuvm on 3/5/18.
 * *
 */
final public class DailyReport {

    private final Map<String, Integer> mDateCountMap = new TreeMap<>();

    public void collect(ChatRoomLog log) {
        int number = 1;

        if (mDateCountMap.containsKey(log.getDate())) {
            number += mDateCountMap.get(log.getDate());
        }

        mDateCountMap.put(log.getDate(), number);
    }

    public Map<String, Integer> getResult() {
        return mDateCountMap;
    }

    public void show() {

        System.out.println("\nDaily Report\n");

        String leftAlignFormat = "| %-15s | %-15d |%n";
        String separate = "+-----------------+-----------------+%n";

        System.out.format(separate);
        System.out.format("| Time            | Number Of ENTER |%n");
        System.out.format(separate);

        mDateCountMap.forEach((s, integer) ->
                System.out.format(leftAlignFormat, s, integer));

        System.out.format(separate);
    }
}
