package reporter.collection;

import model.ChatRoomLog;
import model.DateTimeKey;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by hieuvm on 3/5/18.
 * *
 */
final public class HourlyReport {

    private final Map<DateTimeKey, Integer> mDateCountMap = new TreeMap<>();

    public void collect(ChatRoomLog log) {
        int number = 1;

        DateTimeKey key = generateKey(log);
        if (mDateCountMap.containsKey(key)) {
            number += mDateCountMap.get(key);
        }

        mDateCountMap.put(key, number);
    }

    public void show() {
        System.out.println("\nHourly Report\n");

        String leftAlignFormat = "| %-15s | %-15s | %-15d |%n";
        String separate = "+-----------------+-----------------+-----------------+%n";

        System.out.format(separate);
        System.out.format("| Time            | Hour            | Number Of ENTER |%n");
        System.out.format(separate);
        mDateCountMap.forEach((key, number) ->
                System.out.format(leftAlignFormat, key.getDate(), key.getTime(), number)
        );
        System.out.format(separate);
    }

    private DateTimeKey generateKey(ChatRoomLog log) {
        return new DateTimeKey(log.getDate(), hash(log.getTime()));
    }

    private int hash(String time) {
        try {
            return Integer.parseInt(time.substring(0, 2));
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            return -1;
        }
    }
}
