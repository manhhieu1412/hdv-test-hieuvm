package model;

/**
 * Created by hieuvm on 3/5/18.
 * *
 */
public class ChatRoomLog {

    private String timestamp;
    private String name;
    private String action;

    private String date;
    private String time;

    public ChatRoomLog(String timestamp, String name, String action) {
        this.timestamp = timestamp;
        this.name = name;
        this.action = action;
        analyze();
    }

    private void analyze() {
        String[] splitTime = timestamp.split(" ");
        date = splitTime[0];
        time = splitTime[1];
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

