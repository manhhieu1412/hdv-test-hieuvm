package model;

/**
 * Created by hieuvm on 3/6/18.
 * *
 */
public class DateTimeKey implements Comparable<DateTimeKey> {
    private String date;
    private int time;

    public DateTimeKey(String date, int time) {
        this.date = date;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public int getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DateTimeKey that = (DateTimeKey) o;

        if (time != that.time) return false;
        return date.equals(that.date);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + time;
        return result;
    }

    @Override
    public int compareTo(DateTimeKey o) {
        int result = date.compareTo(o.date);
        if (result == 0) {
            return time - o.time;
        }

        return result;
    }
}
