package main;

import reporter.Reporter;

import java.io.File;
import java.io.IOException;

/**
 * Created by hieuvm on 3/4/18.
 * *
 */
public class Main {
    public static void main(String[] args) {
        Reporter reporter = new Reporter(new File("data.csv"));

        try {
            reporter.run();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
